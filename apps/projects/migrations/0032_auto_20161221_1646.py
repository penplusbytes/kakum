# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0031_auto_20161216_1311'),
    ]

    operations = [
        migrations.RenameField(
            model_name='issuecomment',
            old_name='response_comment',
            new_name='escalation_comment',
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='is_escalated',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='resolution_comment',
            field=tinymce.models.HTMLField(null=True, blank=True),
        ),
    ]
