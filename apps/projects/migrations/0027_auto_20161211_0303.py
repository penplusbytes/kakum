# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0026_auto_20161211_0258'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuecomment',
            name='is_verified',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='responded_to',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='verification_truth',
            field=models.BooleanField(default=False),
        ),
    ]
