# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0030_auto_20161212_0636'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='galleryalbum',
            options={'ordering': ('-id',), 'verbose_name_plural': 'Gallery Album'},
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='district',
            field=models.ForeignKey(related_name='district_comments', blank=True, to='projects.District', null=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='duty_bearer',
            field=models.ForeignKey(related_name='duty_bearer', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='region',
            field=models.ForeignKey(related_name='region_comments', blank=True, to='projects.Region', null=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='response_comment',
            field=tinymce.models.HTMLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='verification_comment',
            field=tinymce.models.HTMLField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='issuecomment',
            name='comment_type',
            field=models.CharField(default=b'Complaint', max_length=100, null=True, blank=True, choices=[(b'Complaint', b'Complaint'), (b'Suggestion', b'Suggestion'), (b'Endorsement', b'Endorsement'), (b'Irrelevant', b'Irrelevant')]),
        ),
    ]
