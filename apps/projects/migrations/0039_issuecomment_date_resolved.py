# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0038_issuecomment_date_verified'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuecomment',
            name='date_resolved',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
