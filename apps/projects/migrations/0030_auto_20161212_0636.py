# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0029_auto_20161212_0635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issuecomment',
            name='author',
            field=models.ForeignKey(related_name='my_comments', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
