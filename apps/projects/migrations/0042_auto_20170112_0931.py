# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0041_auto_20170112_0822'),
    ]

    operations = [
        migrations.RenameField(
            model_name='about',
            old_name='about_us',
            new_name='about',
        ),
    ]
