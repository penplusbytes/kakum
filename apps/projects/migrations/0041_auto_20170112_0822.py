# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0040_aboutus_contact_homepageapproach_newsletter_slider'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='AboutUs',
            new_name='About',
        ),
        migrations.RenameModel(
            old_name='HomepageApproach',
            new_name='Approach',
        ),
        migrations.AlterModelOptions(
            name='slider',
            options={'ordering': ('-id',), 'verbose_name_plural': 'Slider'},
        ),
        migrations.RenameField(
            model_name='news',
            old_name='description',
            new_name='content',
        ),
        migrations.RemoveField(
            model_name='news',
            name='media_url',
        ),
        migrations.RemoveField(
            model_name='news',
            name='project',
        ),
        migrations.RemoveField(
            model_name='news',
            name='show_on_homepage',
        ),
        migrations.AddField(
            model_name='news',
            name='short_content',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
