# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0042_auto_20170112_0931'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsletter',
            name='date_created',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
