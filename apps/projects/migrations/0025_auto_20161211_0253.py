# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0024_auto_20161205_1253'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hospital',
            options={'ordering': ('-id',), 'verbose_name_plural': 'Health Care Service Provider'},
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='hospital',
            field=models.ForeignKey(related_name='hospital_comments', blank=True, to='projects.Hospital', null=True),
        ),
    ]
