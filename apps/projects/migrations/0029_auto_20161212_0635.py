# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0028_issuecomment_phone_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issuecomment',
            name='issue',
            field=models.ForeignKey(related_name='issue_comments', blank=True, to='projects.Issue', null=True),
        ),
    ]
