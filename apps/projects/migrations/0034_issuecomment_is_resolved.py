# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0033_auto_20161221_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuecomment',
            name='is_resolved',
            field=models.BooleanField(default=False),
        ),
    ]
