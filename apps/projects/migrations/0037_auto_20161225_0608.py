# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0036_auto_20161225_0607'),
    ]

    operations = [
        migrations.RenameField(
            model_name='issuecomment',
            old_name='region_escalant',
            new_name='regional_escalant',
        ),
        migrations.RenameField(
            model_name='issuecomment',
            old_name='region_escalation_comment',
            new_name='regional_escalation_comment',
        ),
        migrations.RenameField(
            model_name='issuecomment',
            old_name='region_escalation_date',
            new_name='regional_escalation_date',
        ),
    ]
