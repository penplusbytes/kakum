# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0035_auto_20161225_0527'),
    ]

    operations = [
        migrations.RenameField(
            model_name='issuecomment',
            old_name='is_region_escalated',
            new_name='is_regional_escalated',
        ),
    ]
