# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0037_auto_20161225_0608'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuecomment',
            name='date_verified',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
