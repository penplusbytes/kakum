# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0032_auto_20161221_1646'),
    ]

    operations = [
        migrations.RenameField(
            model_name='issuecomment',
            old_name='escalation_comment',
            new_name='district_escalation_comment',
        ),
        migrations.RenameField(
            model_name='issuecomment',
            old_name='is_escalated',
            new_name='is_district_escalated',
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='district_escalant',
            field=models.ForeignKey(related_name='district_escalant', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='hospital_escalation_comment',
            field=tinymce.models.HTMLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='is_national_escalated',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='is_region_escalated',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='national_escalant',
            field=models.ForeignKey(related_name='national_escalant', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='region_escalant',
            field=models.ForeignKey(related_name='region_escalant', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='region_escalation_comment',
            field=tinymce.models.HTMLField(null=True, blank=True),
        ),
    ]
