# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0025_auto_20161211_0253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issuecomment',
            name='rating',
            field=models.ForeignKey(blank=True, to='projects.CommentCategory', null=True),
        ),
    ]
