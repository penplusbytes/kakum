# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0034_issuecomment_is_resolved'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuecomment',
            name='district_escalation_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='hospital_escalation_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='issuecomment',
            name='region_escalation_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
