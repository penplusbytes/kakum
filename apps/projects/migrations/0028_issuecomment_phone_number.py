# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0027_auto_20161211_0303'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuecomment',
            name='phone_number',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
