# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0023_issuedocument_title'),
    ]

    operations = [
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'Districts',
            },
        ),
        migrations.CreateModel(
            name='Hospital',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('district', models.ForeignKey(related_name='my_hospital', to='projects.District')),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'Hospitals',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('description', tinymce.models.HTMLField(null=True, blank=True)),
                ('image', s3direct.fields.S3DirectField(null=True, blank=True)),
                ('date_published', models.DateField(null=True, blank=True)),
                ('media_url', models.URLField(null=True, blank=True)),
                ('show_on_homepage', models.BooleanField(default=False)),
                ('project', models.ForeignKey(related_name='news', to='projects.Project')),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'News',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('image', s3direct.fields.S3DirectField(null=True, blank=True)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'Regions',
            },
        ),
        migrations.AddField(
            model_name='district',
            name='region',
            field=models.ForeignKey(related_name='my_districts', to='projects.Region'),
        ),
    ]
