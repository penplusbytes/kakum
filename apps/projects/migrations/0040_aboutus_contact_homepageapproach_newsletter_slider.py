# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0039_issuecomment_date_resolved'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('about_us', tinymce.models.HTMLField()),
                ('image', s3direct.fields.S3DirectField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'About Us',
                'verbose_name_plural': 'About Us',
            },
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=100, null=True, blank=True)),
                ('phone', models.CharField(max_length=100, null=True, blank=True)),
                ('address', models.TextField(max_length=1000, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='HomepageApproach',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField(max_length=500)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'HomepageApproach',
            },
        ),
        migrations.CreateModel(
            name='Newsletter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=500)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'Newsletter Subscriptions',
            },
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField(max_length=500)),
                ('image', s3direct.fields.S3DirectField(null=True, blank=True)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'Regions',
            },
        ),
    ]
