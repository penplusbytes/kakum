# coding=utf-8
from django.db import models


class BaseModel(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ('-created_time', )


class PhoneNumbersList(BaseModel):
    name = models.CharField(max_length=128)

    def __str__(self):
        return '%s (%s)' % (self.name, self.phone_numbers.count())

    def phone_numbers_cnt(self):
        return self.phone_numbers.count()


class PhoneNumber(BaseModel):
    phone_numbers_list = models.ForeignKey(PhoneNumbersList, blank=True, null=True, related_name='phone_numbers')
    number = models.CharField(max_length=128, db_index=True)

    def __str__(self):
        return ('%s / %s' % (self.number, self.phone_numbers_list)) if self.phone_numbers_list else ('%s' % self.number)

    def messages_received_from_cnt(self):
        return self.messages_received_from.count()

    def messages_sent_to_cnt(self):
        return self.messages_sent_to.count()


class MessageReceived(BaseModel):
    msg_from = models.ForeignKey(PhoneNumber, related_name='messages_received_from')
    msg_to = models.CharField(max_length=128, blank=True, null=True)
    msg_text = models.CharField(max_length=480, blank=True, null=True)  #more characters?

    def __str__(self):
        return str(self.msg_text)


class MessageSent(BaseModel):
    msg_to = models.ForeignKey(PhoneNumber, related_name='messages_sent_to')
    msg_from = models.CharField(max_length=255)
    msg_text = models.CharField(max_length=480)  #more characters?

    sent_from_backend = models.BooleanField(default=False)
    reply_on = models.OneToOneField(MessageReceived, blank=True, null=True, related_name='replied_by')

    result_status = models.CharField(max_length=128)
    result_content = models.TextField()

    def __str__(self):
        return str(self.msg_text)


class Settings(BaseModel):
    name = models.CharField(max_length=32)
    value = models.CharField(max_length=255)

    class Meta:
        ordering = ('name', )

    @staticmethod
    def get_settings():
        return {s.name: s.value for s in Settings.objects.all()}
