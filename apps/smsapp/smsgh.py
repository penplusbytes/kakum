import urllib
import requests
import json


class SMSGHMessage(object):
    def __init__(self, params):
        self.msg_from = urllib.unquote(params.get('from')) if 'from' in params else None
        self.msg_to = urllib.unquote(params.get('to')) if 'from' in params else None
        self.msg_fulltext = urllib.unquote(params.get('fulltext')) if 'from' in params else None
        self.msg_date = urllib.unquote(params.get('date')) if 'from' in params else None


def send_message(api_url, msg_from, msg_to, msg_content, client_id, client_secret, registered_delivery=False):
    url = '%s?From=%s&To=%s&Content=%s&ClientId=%s&ClientSecret=%s&RegisteredDelivery=%s' % (
        api_url, urllib.quote(msg_from), urllib.quote(msg_to), urllib.quote(msg_content),
        client_id, client_secret, str(registered_delivery).lower()
    )

    response = requests.get(url, headers={'accept': 'application/json'})
    return json.loads(response.content.decode('utf-8'))['Status'], response.content
