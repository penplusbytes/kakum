from django.contrib import admin
from .models import PhoneNumbersList, PhoneNumber, MessageReceived, MessageSent, Settings
from .admin_send_message import send_message


class BaseModelAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time', 'modified_time')
    list_display = ('created_time', 'modified_time')
    list_filter = ('created_time', 'modified_time')


class PhoneNumbersListAdmin(BaseModelAdmin):
    list_display = ('name', 'phone_numbers_cnt') + BaseModelAdmin.list_display

    actions = (send_message,)


class PhoneNumberAdmin(BaseModelAdmin):
    list_display = (
        ('number', 'messages_received_from_cnt', 'messages_sent_to_cnt', 'phone_numbers_list') +
        BaseModelAdmin.list_display
    )
    list_filter = ('phone_numbers_list', )

    actions = (send_message, )


class MessageAdmin(BaseModelAdmin):
    list_display = ('msg_from', 'msg_to', 'msg_text')

    def get_readonly_fields(self, request, obj=None):
        # noinspection PyProtectedMember
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False


class MessageReceivedAdmin(MessageAdmin):
    # noinspection PyMethodMayBeStatic
    def replied_by_msg_text(self, obj):
        return obj.replied_by

    list_display = MessageAdmin.list_display + ('replied_by_msg_text', ) + BaseModelAdmin.list_display


class MessageSentAdmin(MessageAdmin):
    list_display = (
        MessageAdmin.list_display +
        ('sent_from_backend', 'reply_on', 'result_status') +
        BaseModelAdmin.list_display
    )

    list_filter = ('sent_from_backend', ) + MessageAdmin.list_filter


class SettingsAdmin(BaseModelAdmin):
    list_display = ('name', 'value') + BaseModelAdmin.list_display


admin.site.register(PhoneNumbersList, PhoneNumbersListAdmin)
admin.site.register(PhoneNumber, PhoneNumberAdmin)
admin.site.register(MessageReceived, MessageReceivedAdmin)
admin.site.register(MessageSent, MessageSentAdmin)
admin.site.register(Settings, SettingsAdmin)


# noinspection PyProtectedMember
MessageReceived._meta.verbose_name_plural = 'Messages received'

# noinspection PyProtectedMember
MessageSent._meta.verbose_name_plural = 'Messages sent'

# noinspection PyProtectedMember
Settings._meta.verbose_name = 'Setting'
# noinspection PyProtectedMember
Settings._meta.verbose_name_plural = 'Settings'
