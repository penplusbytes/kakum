# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smsapp', '0002_initial_settings'),
    ]

    operations = [
        migrations.AlterField(
            model_name='messagereceived',
            name='msg_text',
            field=models.CharField(max_length=480, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='messagereceived',
            name='msg_to',
            field=models.CharField(max_length=128, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='messagesent',
            name='msg_from',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='messagesent',
            name='msg_text',
            field=models.CharField(max_length=480),
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='number',
            field=models.CharField(max_length=128, db_index=True),
        ),
    ]
