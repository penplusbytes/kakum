# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations
from apps.smsapp.models import Settings


def set_up_initial_settings():
    Settings.objects.bulk_create([
        Settings(name='api_url', value='https://api.smsgh.com/v3/messages/send'),
        Settings(name='autoreply_text', value='Thank you!'),
        Settings(name='client_id', value='xglmolpz'),
        Settings(name='client_secret', value='tvfaerjk'),
        Settings(name='msg_from', value='p4hGhana')
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('smsapp', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(lambda _, __: set_up_initial_settings())
    ]
