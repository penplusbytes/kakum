# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MessageReceived',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True)),
                ('msg_to', models.CharField(null=True, blank=True, max_length=20)),
                ('msg_text', models.CharField(null=True, blank=True, max_length=20)),
            ],
            options={
                'ordering': ('-created_time',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MessageSent',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True)),
                ('msg_from', models.CharField(max_length=200)),
                ('msg_text', models.CharField(max_length=20)),
                ('sent_from_backend', models.BooleanField(default=False)),
                ('result_status', models.CharField(max_length=128)),
                ('result_content', models.TextField()),
            ],
            options={
                'ordering': ('-created_time',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PhoneNumber',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True)),
                ('number', models.CharField(db_index=True, max_length=200)),
            ],
            options={
                'ordering': ('-created_time',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PhoneNumbersList',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=128)),
            ],
            options={
                'ordering': ('-created_time',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=32)),
                ('value', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.AddField(
            model_name='phonenumber',
            name='phone_numbers_list',
            field=models.ForeignKey(blank=True, related_name='phone_numbers', null=True, to='smsapp.PhoneNumbersList'),
        ),
        migrations.AddField(
            model_name='messagesent',
            name='msg_to',
            field=models.ForeignKey(to='smsapp.PhoneNumber', related_name='messages_sent_to'),
        ),
        migrations.AddField(
            model_name='messagesent',
            name='reply_on',
            field=models.OneToOneField(blank=True, related_name='replied_by', null=True, to='smsapp.MessageReceived'),
        ),
        migrations.AddField(
            model_name='messagereceived',
            name='msg_from',
            field=models.ForeignKey(to='smsapp.PhoneNumber', related_name='messages_received_from'),
        ),
    ]
