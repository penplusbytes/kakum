from django.contrib import admin
from django import forms
from django.shortcuts import render, HttpResponseRedirect
from models import PhoneNumber
from apps.smsapp import helpers


class SendMessageForm(forms.Form):
    msg_text = forms.CharField(widget=forms.Textarea)
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)


def send_message(self, request, queryset):
    form = None

    if 'send_message' in request.POST:
        form = SendMessageForm(request.POST)

        if form.is_valid():
            msg_text = form.cleaned_data['msg_text']

            phone_numbers = get_phone_numbers(queryset)

            for phone_number in phone_numbers.all():
                helpers.send_message(phone_number, msg_text, None, True)

            self.message_user(request, 'Message "%s" sent to %s phone number(s)' % (msg_text, len(phone_numbers)))
            return HttpResponseRedirect(request.get_full_path())

    if not form:
        form = SendMessageForm(initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

    return render(request, 'smsapp/send_message.html',
                  {'items': queryset, 'form': form, 'title': 'Send message'})


send_message.short_description = "Send message"


def get_phone_numbers(queryset):
    if queryset.model is PhoneNumber:
        return queryset
    else:
        return PhoneNumber.objects.filter(phone_numbers_list__in=queryset.all())
