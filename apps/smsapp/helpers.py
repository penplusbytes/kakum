from models import MessageSent, Settings
import smsgh


def send_message(phone_number, msg_text, reply_on=None, sent_from_backend=False):
    settings = Settings.get_settings()

    result_status, result_content = smsgh.send_message(
        settings['api_url'], settings['msg_from'], phone_number.number,
        msg_text if msg_text else settings['autoreply_text'],
        settings['client_id'], settings['client_secret']
    )

    MessageSent(
        msg_to=phone_number,
        msg_from=settings['msg_from'],
        msg_text=msg_text if msg_text else settings['autoreply_text'],
        reply_on=reply_on,
        sent_from_backend=sent_from_backend,
        result_status=result_status,
        result_content=result_content
    ).save()


def send_message_single(phone_number, msg_text, reply_on=None, sent_from_backend=False):
    settings = Settings.get_settings()

    result_status, result_content = smsgh.send_message(
        settings['api_url'], settings['msg_from'], phone_number,
        msg_text if msg_text else settings['autoreply_text'],
        settings['client_id'], settings['client_secret']
    )