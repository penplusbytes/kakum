from django.shortcuts import HttpResponse
import django.db.utils
from smsgh import SMSGHMessage
from models import PhoneNumber, MessageReceived
from apps.smsapp import helpers


def receive_message(request):
    smsgh_msg = SMSGHMessage(request.GET)
    try:
        phone_number, _ = PhoneNumber.objects.get_or_create(number=smsgh_msg.msg_from)

        message_received = MessageReceived(
            msg_from=phone_number,
            msg_to=smsgh_msg.msg_to,
            msg_text=smsgh_msg.msg_fulltext
        )
        message_received.save()

        helpers.send_message(phone_number, None, message_received)

        return HttpResponse('ok\n', content_type='text/plain')

    except django.db.utils.IntegrityError:
        return HttpResponse('No "from" number has been set\n', status=400, content_type='text/plain')
