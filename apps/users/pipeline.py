def update_user_social_data(strategy, *args, **kwargs):

    if not kwargs['is_new']:
        return

    # full_name = ''
    backend = kwargs['backend']

    user = kwargs['user']

    # if backend.name == 'twitter':
    #     if kwargs.get('details'):
    #         full_name = kwargs['details'].get('fullname')

    # user.first_name = full_name

    if backend.name == 'facebook':
        fbuid = kwargs['response']['id']
        image_url = 'http://graph.facebook.com/%s/picture?type=large' % fbuid
        user.sm_avatar = image_url

    if backend.name == 'twitter':
        if kwargs['response'].get('profile_image_url'):
            image_url = kwargs['response'].get('profile_image_url')
            user.sm_avatar = str(image_url.split('_normal')[0]) + '.jpg'
            user.signup_method = 'Twitter'
            user.is_p4h_user = True

    user.save()
