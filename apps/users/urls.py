from django.conf.urls import patterns, url
import os


if 'P4H' in os.environ:
    urlpatterns = patterns('',
        url(r'^logout/$', 'apps.users.views.logout_view', name="logout"),
        url(r'^p4h_backend_login/$', 'apps.users.views.p4h_login_view', name="login"),
        url(r'^p4h_dashboard/$', 'apps.users.views.p4h_dashboard', name="p4h_dashboard"),
        url(r'^newsletter_emails/$', 'apps.users.views.newsletter_emails', name="newsletter_emails"),
        url(r'^export_emails/$', 'apps.users.views.export_emails', name="export_emails"),
        url(r'^edit_p4h_user/(?P<pk>\d+)/$', 'apps.users.views.edit_p4h_user', name="edit_p4h_user"),
        url(r'^user_detail/(?P<pk>\d+)/$', 'apps.users.views.p4h_user_detail', name="user_detail"),
        url(r'^create_p4h_users/$', 'apps.users.views.create_p4h_user', name="create_p4h_user"),
        url(r'^edit_p4h_users/(?P<pk>\d+)/$', 'apps.users.views.edit_p4h_users_p4h', name="edit_p4h_user"),
        url(r'^create_p4h_admin/$', 'apps.users.views.create_p4h_admin', name="create_p4h_admin"),
        url(r'^edit_p4h_admin/(?P<pk>\d+)/$', 'apps.users.views.edit_p4h_admin', name="edit_p4h_admin"),
        url(r'^all_p4h_admin/$', 'apps.users.views.p4h_admin_list', name="p4h_admin_list"),
        url(r'^p4h_registered_users/$', 'apps.users.views.p4h_registered_users', name="registered_users"),
        url(r'^confirm_delete_admin/(?P<pk>\d+)/$', 'apps.users.views.confirm_delete_p4h_admin', name="confirm_delete_admin"),
        url(r'^confirm_deactivate_admin/(?P<pk>\d+)/$', 'apps.users.views.confirm_deactivate_admin', name="confirm_deactivate_admin"),
        url(r'^delete_admin/(?P<pk>\d+)/$', 'apps.users.views.delete_p4h_admin', name="delete_admin"),
        url(r'^deactivate_admin/(?P<pk>\d+)/$', 'apps.users.views.deactivate_p4h_admin', name="deactivate_admin"),
    )

else:
    urlpatterns = patterns('',
        url(r'^login/$', 'apps.users.views.login_view', name="login"),
        url(r'^logout/$', 'apps.users.views.logout_view', name="logout"),
        url(r'^dashboard/$', 'apps.users.views.dashboard', name="dashboard"),
        url(r'^messages_received/$', 'apps.users.views.messages_received', name="messages_received"),
        url(r'^all_admin/$', 'apps.users.views.admin_list', name="admin_list"),
        url(r'^users_list/$', 'apps.users.views.user_list', name="user_list"),
        url(r'^user_detail/(?P<pk>\d+)/$', 'apps.users.views.user_detail', name="user_detail"),
        url(r'^delete_admin/(?P<pk>\d+)/$', 'apps.users.views.delete_admin', name="delete_admin"),
        url(r'^confirm_delete_admin/(?P<pk>\d+)/$', 'apps.users.views.confirm_delete_admin', name="confirm_delete_admin"),
        url(r'^create_admin/$', 'apps.users.views.create_admin', name="create_admin"),
        url(r'^create_p4h_users/$', 'apps.users.views.create_p4h_users', name="create_p4h_users"),
        url(r'^edit_admin/(?P<pk>\d+)/$', 'apps.users.views.edit_admin', name="edit_admin"),
        url(r'^edit_p4h_users/(?P<pk>\d+)/$', 'apps.users.views.edit_p4h_users', name="edit_p4h_users"),
    )
