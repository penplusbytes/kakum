# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0023_auto_20161210_1255'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kakuser',
            name='is_admin',
        ),
        migrations.AddField(
            model_name='kakuser',
            name='user_permission',
            field=models.CharField(default=b'0', max_length=100, choices=[(b'1', b'Super User'), (b'0', b'Normal User'), (b'2', b'Staff User')]),
        ),
        migrations.AddField(
            model_name='kakuser',
            name='is_admin',
            field=models.BooleanField(default=False),
        ),
    ]
