# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0030_kakuser_signup_method'),
    ]

    operations = [
        migrations.AddField(
            model_name='kakuser',
            name='age',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
