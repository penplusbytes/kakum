# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0029_kakuser_is_p4h_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='kakuser',
            name='signup_method',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
    ]
