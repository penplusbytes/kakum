# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0025_auto_20161216_1311'),
    ]

    operations = [
        migrations.RenameField(
            model_name='kakuser',
            old_name='regional_id',
            new_name='region_id',
        ),
    ]
