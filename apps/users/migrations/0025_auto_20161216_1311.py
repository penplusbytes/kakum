# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0024_auto_20161212_0505'),
    ]

    operations = [
        migrations.AddField(
            model_name='kakuser',
            name='district_id',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='kakuser',
            name='hospital_id',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='kakuser',
            name='phone_number',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='kakuser',
            name='regional_id',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='kakuser',
            name='user_permission',
            field=models.CharField(default=b'0', max_length=100, choices=[(b'0', b'Normal User'), (b'1', b'Super User'), (b'2', b'Staff User'), (b'3', b'Duty Bearer - P4H'), (b'4', b'District Monitor - P4H'), (b'5', b'Regional Monitor - P4H'), (b'6', b'National Monitor - P4H')]),
        ),
    ]
