# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0027_kakuser_unique_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kakuser',
            name='user_permission',
            field=models.CharField(default=b'0', max_length=100, choices=[(b'0', b'Normal User - Kakum'), (b'1', b'Super User - Kakum'), (b'2', b'Staff User - Kakum'), (b'3', b'Duty Bearer - P4H'), (b'4', b'District Health Management Team - P4H'), (b'5', b'Regional Monitor - P4H'), (b'6', b'National Monitor - P4H'), (b'7', b'Super User - P4H')]),
        ),
    ]
