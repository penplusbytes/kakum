# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0028_auto_20170111_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='kakuser',
            name='is_p4h_user',
            field=models.BooleanField(default=False),
        ),
    ]
