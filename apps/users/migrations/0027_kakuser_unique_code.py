# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0026_auto_20161218_0338'),
    ]

    operations = [
        migrations.AddField(
            model_name='kakuser',
            name='unique_code',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
    ]
