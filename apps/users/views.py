from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from forms import LoginForm, DivErrorList, PasswordChangeForm
from django.http import Http404
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.csrf import csrf_protect
from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db.models import Q
from apps.users.models import *
from apps.projects.models import *
from apps.smsapp.models import MessageReceived
from forms import *
from datetime import datetime
from django.core.mail import send_mail
import openpyxl
from openpyxl.utils import get_column_letter
from django.http import HttpResponse
from random import randrange
from twilio.rest import TwilioRestClient
from cryptography.fernet import Fernet
from apps.smsapp.helpers import send_message_single


def unique_code():
    CHARSET = '0123456789'
    LENGTH = 6
    new_code = ''
    for i in xrange(LENGTH):
        new_code += CHARSET[randrange(0, len(CHARSET))]
    return new_code


@login_required
def logout_view(request):
  logout(request)
  return redirect('/')


@login_required
def dashboard(request):
    if request.user.user_permission == '1' or request.user.user_permission == '2':
        if request.user.is_authenticated():
            today = datetime.today()
            projects = Project.objects.all()
            num_of_users = KAKUser.objects.filter(user_permission='0').count()
            num_of_issues = Issue.objects.all().count()
            num_of_comments = IssueComment.objects.all().count()
            project_name = ''
            if request.user.user_permission == '2':
                project_name = Project.objects.get(id=request.user.project_id)
                num_of_users = IssueComment.objects.filter(issue__project__id=int(request.user.project_id), author__user_permission='0').count()
                num_of_issues = Issue.objects.filter(project__id=int(request.user.project_id)).count()
                num_of_comments = IssueComment.objects.filter(issue__project__id=int(request.user.project_id)).count()
            issues = ''
            if request.user.user_permission == '2':
                issues = Issue.objects.filter(project__id=int(request.user.project_id))
            web_count = IssueComment.objects.filter(timestamp__year=today.year, timestamp__month=today.month,
                                                               timestamp__day=today.day, input_channel='web').count()
            results = IssueComment.objects.filter(timestamp__year=today.year, timestamp__month=today.month,
                                                        timestamp__day=today.day)
            social_count = results.filter(Q(input_channel='Facebook')|
                                         Q(input_channel='Twitter')|
                                         Q(input_channel='Whatsapp')).count()
            offline_count = IssueComment.objects.filter(timestamp__year=today.year, timestamp__month=today.month,
                                                               timestamp__day=today.day, input_channel='Offline').count()
            latest = IssueComment.objects.all().order_by('-id')[:5]
            return render(request, 'index.html', {
                          'latest': latest,
                          'num_of_projects': projects.count(),
                          'num_of_users':num_of_users,
                          'num_of_issues': num_of_issues,
                          'num_of_comments': num_of_comments,
                          'issues': issues,
                          'web_count': web_count,
                          'social_count': social_count,
                          'offline_count': offline_count,
                          'project_name': project_name,
                          'projects': projects})
        redirect('/login/')
    else:
        raise Http404


def login_view(request):
    next = request.GET.get('next')
    if request.user.is_authenticated():
        redirect('/users/dashboard/')

    if request.method == "POST":
        form = LoginForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            userobject = authenticate(email=email, password=password)
            if userobject is not None and userobject.is_active:
                login(request, userobject)
                if request.POST.get('next'):
                    return redirect(request.POST.get('next'))
                return redirect('/users/dashboard/')
            else:
                return render(request, 'home.html', {'form': form})
        else:
            return render(request, 'home.html', {'form': form})

    ''' user is not submitting any form, show the login form '''
    form = LoginForm()
    return render(request, 'home.html', {'form': form, 'next': next})


@sensitive_post_parameters()
@csrf_protect
@login_required
def password_change(request,
                    template_name='registration/password_change_form.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm,
                    current_app=None, extra_context=None):
    if post_change_redirect is None:
        post_change_redirect = reverse('password_change_done')
    else:
        post_change_redirect = resolve_url(post_change_redirect)
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(post_change_redirect)
    else:
        form = password_change_form(user=request.user)
    context = {
        'form': form,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


@login_required
def password_change_done(request,
                         template_name='registration/password_change_done.html',
                         current_app=None, extra_context=None):
    context = {}
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context, current_app=current_app)


@login_required
def user_list(request):
    if request.user.user_permission == '1' or request.user.user_permission == '2':
        myusers = KAKUser.objects.filter(is_active=True, user_permission='0').order_by('-id')
        if request.method == 'GET':
            return render_to_response('userlist.html', {'myusers': myusers,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def admin_list(request):
    if request.user.user_permission == '1' or request.user.user_permission == '2':
        myusers = KAKUser.objects.filter(is_active=True).exclude(user_permission='0').order_by('-id')
        if request.method == 'GET':
            return render_to_response('user_list.html', {'myusers': myusers,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def user_detail(request, pk):
    if request.user.user_permission == '1' or request.user.user_permission == '2':
        myuser = KAKUser.objects.get(id=pk)
        if request.method == 'GET':
            return render_to_response('user_detail.html', {'myuser': myuser,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def confirm_delete_admin(request, pk):
    if request.user.user_permission == '1':
        myuser = KAKUser.objects.get(id=pk)
        if request.method == 'GET':
            return render_to_response('confirm_delete_admin.html', {'id': pk, 'myuser': myuser,
                }, context_instance=RequestContext(request))
    else:
        redirect('/users/dashboard/')


@login_required
def confirm_deactivate_admin(request, pk):
    if request.user.user_permission == '7':
        myuser = KAKUser.objects.get(id=pk)
        if request.method == 'GET':
            return render_to_response('p4h/confirm_delete_admin.html', {'id': pk, 'myuser': myuser,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def confirm_delete_p4h_admin(request, pk):
    if request.user.user_permission == '7':
        myuser = KAKUser.objects.get(id=pk)
        if request.method == 'GET':
            return render_to_response('p4h/confirm_delete_admin_proper.html', {'id': pk, 'myuser': myuser,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def delete_admin(request, pk):
    if request.user.user_permission == '1':
        if request.method == 'POST':
            myuser = KAKUser.objects.get(id=pk)
            myuser.is_active = False
            myuser.save()
            return redirect('/users/all_admin/?deleted=True')
    else:
        raise Http404


@login_required
def delete_p4h_admin(request, pk):
    if request.user.user_permission == '7':
        if request.method == 'POST':
            KAKUser.objects.get(id=pk).delete()
            return redirect('/users/all_admin/?deleted=True')
    else:
        raise Http404


@login_required
def deactivate_p4h_admin(request, pk):
    if request.user.user_permission == '7':
        if request.method == 'POST':
           myuser = KAKUser.objects.get(id=pk)
           myuser.is_active = False
           myuser.save()
           return redirect('/users/all_p4h_admin/?deleted=True')
    else:
        raise Http404


@login_required
def create_admin(request):
    if request.user.user_permission == '1':
        if request.method == 'GET':
            projects = Project.objects.all()
            form = AdminForm(error_class=DivErrorList, options=projects)
            return render_to_response("add_admin.html", {
                "form": form,
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            projects = Project.objects.all()
            form = AdminForm(request.POST, error_class=DivErrorList, options=projects)
            if form.is_valid():
                my_user = form.save(commit=False)
                if request.POST['user_permission'] == '2':
                    my_user.is_superuser = True
                my_user.save()
                return redirect('/users/all_admin/?added=True')
            else:
                return render_to_response("add_admin.html", {
                    "form": form,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def edit_admin(request, pk):
    if request.user.user_permission == '1':
        if request.method == 'GET':
            myinstance = KAKUser.objects.get(id=int(pk))
            project = Project.objects.get(id=int(myinstance.project_id))
            projects = Project.objects.all()
            form = AdminEditForm(error_class=DivErrorList, instance=myinstance, options=projects)
            return render_to_response("edit_admin.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
                'project': project.name
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            myinstance = KAKUser.objects.get(id=int(pk))
            projects = Project.objects.all()
            form = AdminEditForm(request.POST or None, error_class=DivErrorList, instance=myinstance, options=projects)
            if form.is_valid():
                my_user = form.save(commit=False)
                my_user.save()
                return redirect('/users/all_admin/?edited=True')
            else:
                return render_to_response("edit_admin.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
                }, context_instance=RequestContext(request))
    else:
        raise Http404


def handler500(request):
    return render(request, '500.html')


def handler404(request):
    return render(request, '404.html')

###### this is for kakum
@login_required
def create_p4h_users(request):
    if request.user.user_permission == '1':
        if request.method == 'GET':
            regions = Region.objects.all()
            form = P4HAdminForm(error_class=DivErrorList, regions=regions)
            return render_to_response("add_p4h_admin.html", {
                "form": form,
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            regions = Region.objects.all()
            form = P4HAdminForm(request.POST, error_class=DivErrorList, regions=regions)
            if form.is_valid():
                my_user = form.save(commit=False)
                my_user.save()
                return redirect('/users/all_admin/?added=True')
            else:
                return render_to_response("add_p4h_admin.html", {
                    "form": form,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


##### this is for P4H dashboard
@login_required
def create_p4h_user(request):
    if request.user.user_permission == '7':

        if request.method == 'GET':
            regions = Region.objects.all()
            form = P4HAdminForm(error_class=DivErrorList, regions=regions)
            return render_to_response("p4h/add_p4h_user.html", {
                "form": form,
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            regions = Region.objects.all()
            form = P4HAdminForm(request.POST, error_class=DivErrorList, regions=regions)
            if form.is_valid():
                my_user = form.save(commit=False)
                my_user.save()
                return redirect('/users/all_p4h_admin/?added=True')
            else:
                return render_to_response("p4h/add_p4h_user.html", {
                    "form": form,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def p4h_admin_list(request):
    if request.user.user_permission == '7':
        myusers = KAKUser.objects.filter(is_active=True).exclude(user_permission='0').exclude(user_permission='1').exclude(user_permission='2').order_by('-id')
        if request.method == 'GET':
            return render_to_response('p4h/user_list.html', {'myusers': myusers,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def create_p4h_admin(request):
    if request.user.user_permission == '7':
        if request.method == 'GET':
            form = P4HSuperAdminForm(error_class=DivErrorList)
            return render_to_response("p4h/add_admin.html", {
                "form": form,
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            form = P4HSuperAdminForm(request.POST, error_class=DivErrorList)
            if form.is_valid():
                my_user = form.save(commit=False)
                my_user.save()
                return redirect('/users/all_p4h_admin/?added=True')
            else:
                return render_to_response("p4h/add_admin.html", {
                    "form": form,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def edit_p4h_admin(request, pk):
    if request.user.user_permission == '7':
        if request.method == 'GET':
            myinstance = KAKUser.objects.get(id=int(pk))
            form = P4HSuperAdminEditForm(error_class=DivErrorList, instance=myinstance)
            return render_to_response("p4h/add_admin.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            myinstance = KAKUser.objects.get(id=int(pk))
            form = P4HSuperAdminEditForm(request.POST or None, error_class=DivErrorList, instance=myinstance)
            if form.is_valid():

                my_user = form.save(commit=False)
                my_user.save()
                return redirect('/users/all_p4h_admin/?edited=True')
            else:
                return render_to_response("p4h/add_admin.html", {
                    "form": form,
                    'id': pk,
                    'edit': 'True',
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def edit_p4h_users(request, pk):
    if request.user.user_permission == '1':
        if request.method == 'GET':
            myinstance = KAKUser.objects.get(id=int(pk))
            form = P4HAdminEditForm(error_class=DivErrorList, instance=myinstance)
            return render_to_response("edit_p4h_admin.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            myinstance = KAKUser.objects.get(id=int(pk))            
            form = P4HAdminEditForm(request.POST or None, error_class=DivErrorList, instance=myinstance)
            if form.is_valid():
                my_user = form.save(commit=False)
                my_user.save()
                return redirect('/users/all_admin/?edited=True')
            else:
                return render_to_response("edit_p4h_admin.html", {
                    "form": form,
                    'id': pk,
                    'edit': 'True',
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def edit_p4h_users_p4h(request, pk):
    if request.user.user_permission == '7':
        if request.method == 'GET':
            myinstance = KAKUser.objects.get(id=int(pk))
            regions = Region.objects.all()
            form = P4HAdminEditForm(error_class=DivErrorList, regions=regions, instance=myinstance)
            return render_to_response("p4h/add_p4h_user.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            myinstance = KAKUser.objects.get(id=int(pk))
            old_permission = myinstance.user_permission
            old_region_id = myinstance.region_id
            old_district_id = myinstance.district_id
            old_hospital_id = myinstance.hospital_id

            regions = Region.objects.all()
            form = P4HAdminEditForm(request.POST or None, error_class=DivErrorList, regions=regions, instance=myinstance)
            if form.is_valid():
                my_user = form.save(commit=False)

                if old_permission == my_user.user_permission:
                    region = request.POST['region']
                    district = request.POST['district']
                    hospital = request.POST['hospital']

                    if my_user.user_permission == '3':
                        if old_region_id != region:
                            my_user.region_id = Region.objects.get(id=int(region)).id

                        if old_district_id != district and district != '':
                            my_user.district_id = District.objects.get(id=int(district)).id

                        if old_hospital_id != hospital and hospital != '':
                            my_user.hospital_id = Hospital.objects.get(id=int(hospital)).id

                    elif my_user.user_permission == '4':
                        if old_region_id != region:
                            my_user.region_id = Region.objects.get(id=int(region)).id

                        if old_district_id != district and district != '':
                            my_user.district_id = District.objects.get(id=int(district)).id
                    elif my_user.user_permission == '5':
                        if old_region_id != region:
                            my_user.region_id = Region.objects.get(id=int(region)).id


                elif old_permission != my_user.user_permission:
                    region = request.POST['region']
                    district = request.POST['district']
                    hospital = request.POST['hospital']
                    if my_user.user_permission == '3':
                        my_user.region_id = Region.objects.get(id=int(region)).id
                        my_user.district_id = District.objects.get(id=int(district)).id
                        my_user.hospital_id = Hospital.objects.get(id=int(hospital)).id
                    elif my_user.user_permission == '4':
                        my_user.region_id = Region.objects.get(id=int(region)).id
                        my_user.district_id = District.objects.get(id=int(district)).id
                    elif my_user.user_permission == '5':
                        my_user.region_id = Region.objects.get(id=int(region)).id
                my_user.save()
                return redirect('/users/all_p4h_admin/?edited=True')
            else:
                return render_to_response("p4h/add_p4h_user.html", {
                    "form": form,
                    'id': pk,
                    'edit': 'True',
                }, context_instance=RequestContext(request))
    else:
        raise Http404

###################################### P4H THINGS #############################################
def p4h_login_view(request):
    next = request.GET.get('next')
    if request.user.is_authenticated():
        redirect('/users/p4h_dashboard/')

    if request.method == "POST":
        form = LoginForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            key = '-3lXW2sR7jQvVKQHRP439TsNTCq1hRLinxaUbcNG6lE='
            cipher_suite = Fernet(key)
            userobject = authenticate(email=email, password=password)
            if userobject is not None and userobject.is_active:
                email_id = cipher_suite.encrypt(str(password))
                userobject.unique_code = unique_code()
                userobject.save()
                msg = 'Enter this code %s to complete the login on People 4 Health' % userobject.unique_code
                send_message_single(userobject.phone_number, msg, None, True)
                return render(request, 'p4h/enter_2fa.html', {'next': request.POST.get('next'),
                    'email_id': email_id,
                    'phone_number': userobject.phone_number[-4:]
                })
            else:
                return render(request, 'p4h/backend_login.html', {'form': form})
        else:
            return render(request, 'p4h/backend_login.html', {'form': form})

    form = LoginForm()
    return render(request, 'p4h/backend_login.html', {'form': form, 'next': next})


def twofa_confirm(request):
    if request.method == "POST":
        unique_code = request.POST['unique_code']
        password = request.POST['email_id']
        try:
            myuser = KAKUser.objects.get(unique_code=unique_code)
            key = '-3lXW2sR7jQvVKQHRP439TsNTCq1hRLinxaUbcNG6lE='
            cipher_suite = Fernet(key)
            userobject = authenticate(email=myuser.email, password=cipher_suite.decrypt(str(password)))
            login(request, userobject)
            if request.POST.get('next') is not None and request.POST.get('next') != 'None' and request.POST.get('next') != '':
                return redirect(request.POST.get('next'))
            return redirect('/users/p4h_dashboard/')
        except:
            return redirect('/backend/?wrong_code=True')


@login_required
def p4h_dashboard(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':
        if request.user.is_authenticated():
            if request.user.user_permission == '3':
                total_comments = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id)).count()
                latest_comments = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id))[:10]
                verified_comments = IssueComment.objects.filter(is_verified=True, hospital__id=int(request.user.hospital_id)).count()
                unverified_comments = IssueComment.objects.filter(is_verified=False, hospital__id=int(request.user.hospital_id)).count()
                unresolved_comments = IssueComment.objects.filter(responded_to=False, hospital__id=int(request.user.hospital_id)).count()

                return render(request, 'p4h/dashboard.html', {
                              'total_comments': total_comments,
                              'verified_comments': verified_comments,
                              'unverified_comments': unverified_comments,
                              'unresolved_comments': unresolved_comments,
                              'latest_comments': latest_comments,
                              })
            elif request.user.user_permission == '4':
                total_comments = IssueComment.objects.filter(district__id=int(request.user.district_id)).count()
                latest_comments = IssueComment.objects.filter(district__id=int(request.user.district_id))[:10]
                number_of_hospitals = Hospital.objects.filter(district__id=int(request.user.district_id)).count()
                unverified_comments = IssueComment.objects.filter(is_verified=False, district__id=int(request.user.district_id)).count()
                unresolved_comments = IssueComment.objects.filter(responded_to=False, district__id=int(request.user.district_id)).count()

                return render(request, 'p4h/dashboard.html', {
                              'total_comments': total_comments,
                              'number_of_hospitals': number_of_hospitals,
                              'unverified_comments': unverified_comments,
                              'unresolved_comments': unresolved_comments,
                              'latest_comments': latest_comments,
                              })
            elif request.user.user_permission == '5':
                total_comments = IssueComment.objects.filter(region__id=int(request.user.region_id)).count()
                latest_comments = IssueComment.objects.filter(region__id=int(request.user.region_id))[:10]
                number_of_hospitals = Hospital.objects.filter(region__id=int(request.user.region_id)).count()
                unverified_comments = IssueComment.objects.filter(is_verified=False, region__id=int(request.user.region_id)).count()
                number_of_districts = District.objects.filter(is_verified=True, region__id=int(request.user.region_id)).count()

                return render(request, 'p4h/dashboard.html', {
                              'total_comments': total_comments,
                              'number_of_hospitals': number_of_hospitals,
                              'unverified_comments': unverified_comments,
                              'number_of_districts': number_of_districts,
                              'latest_comments': latest_comments,
                              })

            elif request.user.user_permission == '6' or request.user.user_permission == '7':
                total_comments = IssueComment.objects.filter(issue__project__id=3).count()
                number_of_hospitals = Hospital.objects.all().count()
                number_of_regions = Region.objects.all().count()
                number_of_districts = District.objects.all().count()
                latest_comments = IssueComment.objects.filter(issue__project__id=3)[:10]

                return render(request, 'p4h/dashboard.html', {
                              'total_comments': total_comments,
                              'number_of_hospitals': number_of_hospitals,
                              'number_of_regions': number_of_regions,
                              'number_of_districts': number_of_districts,
                              'latest_comments': latest_comments,
                              })

        redirect('/backend/')
    else:
        raise Http404


#this is for my account in the menu
@login_required
def edit_p4h_user(request, pk):
    if request.user.user_permission == '7' or request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':
        if request.user.id == int(pk):
            if request.method == 'GET':
                myinstance = KAKUser.objects.get(id=int(pk))
                form = P4HAdminAccountEditForm(error_class=DivErrorList, instance=myinstance)
                return render_to_response("p4h/edit_p4h_admin.html", {
                    "form": form,
                    'id': pk,
                    'edit': 'True',
                }, context_instance=RequestContext(request))

            if request.method == 'POST':
                myinstance = KAKUser.objects.get(id=int(pk))
                form = P4HAdminAccountEditForm(request.POST or None, error_class=DivErrorList, instance=myinstance)
                if form.is_valid():
                    my_user = form.save(commit=False)
                    my_user.save()
                    return redirect('/users/user_detail/%s' % pk)
                else:
                    return render_to_response("p4h/edit_p4h_admin.html", {
                        "form": form,
                        'id': pk,
                        'edit': 'True',
                    }, context_instance=RequestContext(request))
        else:
            raise Http404
    else:
        raise Http404


@login_required
def p4h_user_detail(request, pk):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6':
        if request.user.id == int(pk):
            myuser = KAKUser.objects.get(id=pk)
            if request.method == 'GET':
                return render_to_response('p4h/user_detail.html', {'myuser': myuser,
                    'mycomments': IssueComment.objects.filter(author__id=int(pk)),
                    }, context_instance=RequestContext(request))
        else:
            raise Http404
    elif request.user.user_permission == '7':
        myuser = KAKUser.objects.get(id=pk)
        if request.method == 'GET':
            return render_to_response('p4h/user_detail.html', {'myuser': myuser,
                'mycomments': IssueComment.objects.filter(author__id=int(pk)),
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def messages_received(request):
    if request.user.user_permission == '1':
        mymessages = MessageReceived.objects.all().order_by('-id')
        return render_to_response('messages_received.html', {
                    'mymessages': mymessages, 
                    'total_messages': mymessages.count(),
                }, context_instance=RequestContext(request))
    else:
        raise Http404



@login_required
def p4h_registered_users(request):
    if request.user.user_permission == '7':
        myusers = KAKUser.objects.filter(is_p4h_user=True).order_by('-id')
        if request.method == 'GET':
            return render_to_response('p4h/registered_users.html', {'myusers': myusers,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def newsletter_emails(request):
    if request.user.user_permission == '7':
        myemails = Newsletter.objects.all().order_by('-id')
        if request.method == 'GET':
            return render_to_response('p4h/newsletter_emails.html', {'myemails': myemails,
                }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def export_emails(request):
    if request.user.user_permission == '7':
        newsletters = Newsletter.objects.all()
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=newsletter_emails.xlsx'
        wb = openpyxl.Workbook()
        ws = wb.get_active_sheet()
        ws.title = "Emails"

        row_num = 0

        columns = [
            (u"No", 10),
            (u"Email", 35),
        ]

        for col_num in xrange(len(columns)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            c.value = columns[col_num][0]
            # c.style.font.bold = True
            # set column width
            ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

        for item in newsletters:
            row_num += 1
            row = [
                row_num,
                item.email,
            ]
            for col_num in xrange(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                c.value = row[col_num]

        wb.save(response)
        return response
    else:
        raise Http404
