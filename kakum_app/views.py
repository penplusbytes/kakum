from django.shortcuts import render, redirect, render_to_response
from apps.users.forms import LoginForm, DivErrorList
from apps.projects.forms import P4HStaffCommentForm, P4HResolveForm, P4HNationalEscalateForm, P4HRegionalEscalateForm, P4HDistrictEscalateForm, P4HCommentVerifyForm
from apps.users.models import KAKUser
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from apps.projects.models import *
from django.core.mail import send_mail
from django.template import RequestContext
from datetime import datetime
from django.http import Http404
from apps.smsapp.helpers import send_message_single


def backend_home(request):
    if request.user.is_authenticated():
        return redirect('/users/dashboard/')
    next = request.GET.get('next')
    form = LoginForm()
    return render(request, 'home.html', {'form': form, 'next': next})


@login_required
def issues_visualisations(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':
        if request.user.user_permission == '3':
            issues = Issue.objects.filter(project__id=3)
            num_of_issues = Issue.objects.filter(project__id=3).count()
            num_of_comments = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id)).count()
            verified_comments = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id)).count()
            channel_graph = {
                    'Web': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='Web').count(),
                    'SMS': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='SMS').count(),
                    'Email': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='Email').count(),
                    'WhatsApp': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='WhatsApp').count(),
                    'Facebook': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='Facebook').count(),
                    'Twitter': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='Twitter').count(),
                    'Offline': IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), input_channel='Offline').count(),
            }
            num_of_unresolved = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), responded_to=False).count()
            num_of_resolved = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id), responded_to=True).count()
            latest = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id)).order_by('-id')[:5]

        elif request.user.user_permission == '4':
            issues = Issue.objects.filter(project__id=3)
            num_of_issues = Issue.objects.filter(project__id=3).count()
            num_of_comments = IssueComment.objects.filter(district__id=int(request.user.district_id)).count()
            verified_comments = IssueComment.objects.filter(district__id=int(request.user.district_id)).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', district__id=int(request.user.district_id)).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', district__id=int(request.user.district_id)).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', district__id=int(request.user.district_id)).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', district__id=int(request.user.district_id)).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', district__id=int(request.user.district_id)).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', district__id=int(request.user.district_id)).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', district__id=int(request.user.district_id)).count(),
            }

            num_of_unresolved = IssueComment.objects.filter(responded_to=False, district__id=int(request.user.district_id)).count()
            num_of_resolved = IssueComment.objects.filter(responded_to=True, district__id=int(request.user.district_id)).count()
            latest = IssueComment.objects.filter(district__id=int(request.user.district_id)).order_by('-id')[:5]

        elif request.user.user_permission == '5':
            issues = Issue.objects.filter(project__id=3)
            num_of_issues = Issue.objects.filter(project__id=3).count()
            num_of_comments = IssueComment.objects.filter(region__id=int(request.user.region_id)).count()
            verified_comments = IssueComment.objects.filter(region__id=int(request.user.region_id)).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', region__id=int(request.user.region_id)).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', region__id=int(request.user.region_id)).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', region__id=int(request.user.region_id)).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', region__id=int(request.user.region_id)).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', region__id=int(request.user.region_id)).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', region__id=int(request.user.region_id)).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', region__id=int(request.user.region_id)).count(),
            }

            num_of_unresolved = IssueComment.objects.filter(issue_resolved=False, region__id=int(request.user.region_id)).count()
            num_of_resolved = IssueComment.objects.filter(issue_resolved=True, region__id=int(request.user.region_id)).count()
            latest = IssueComment.objects.filter(region__id=int(request.user.region_id)).order_by('-id')[:5]


        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            issues = Issue.objects.filter(project__id=3)
            num_of_issues = Issue.objects.filter(project__id=3).count()
            num_of_comments = IssueComment.objects.filter(issue__project__id=3).count()
            verified_comments = IssueComment.objects.filter(issue__project__id=3, is_verified=True).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', issue__project__id=3).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', issue__project__id=3).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', issue__project__id=3).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', issue__project__id=3).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', issue__project__id=3).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', issue__project__id=3).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', issue__project__id=3).count(),
            }

            num_of_unresolved = IssueComment.objects.filter(responded_to=False, issue__project__id=3).count()
            num_of_resolved = IssueComment.objects.filter(responded_to=True, issue__project__id=3).count()
            latest = IssueComment.objects.filter(issue__project__id=3).order_by('-id')[:5]
        comment_graph = {}
        if num_of_comments > 0:
            for item in issues:
                number_of_comments = item.num_of_comments()
                mykey = '%s (%s)' % (item.title, number_of_comments)
                comment_graph[mykey] = (float(number_of_comments) / 100) * num_of_comments

        return render(request, 'p4h/issues_visualisation.html', {
                      'latest': latest,
                      'num_of_issues': num_of_issues,
                      'num_of_comments': num_of_comments,
                      'num_of_resolved': num_of_resolved,
                      'num_of_unresolved': num_of_unresolved,
                      'issues': issues,
                      'verified_comments': verified_comments,
                      'comment_graph': comment_graph,
                      'channel_graph': channel_graph}
                      )


@login_required
def issues_visualisations_details(request, pk):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':
        if request.user.user_permission == '3':
            issues = Issue.objects.filter(project__id=3)
            num_of_comments = IssueComment.objects.filter(issue__id=int(pk), hospital__id=int(request.user.hospital_id)).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count(),
            }
            num_of_unresolved = IssueComment.objects.filter(issue_resolved=False, hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count()
            num_of_resolved = IssueComment.objects.filter(issue_resolved=True, hospital__id=int(request.user.hospital_id), issue__id=int(pk)).count()
            latest = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id)).order_by('-id')[:5]

        elif request.user.user_permission == '4':
            issues = Issue.objects.filter(project__id=3)
            num_of_comments = IssueComment.objects.filter(issue__id=int(pk), district__id=int(request.user.district_id)).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', district__id=int(request.user.district_id), issue__id=int(pk)).count(),
            }
            
            
            num_of_unresolved = IssueComment.objects.filter(responded_to=False, district__id=int(request.user.district_id), issue__id=int(pk)).count()
            num_of_resolved = IssueComment.objects.filter(responded_to=True, district__id=int(request.user.district_id), issue__id=int(pk)).count()
            latest = IssueComment.objects.filter(district__id=int(request.user.district_id)).order_by('-id')[:5]


        elif request.user.user_permission == '5':
            issues = Issue.objects.filter(project__id=3)
            num_of_comments = IssueComment.objects.filter(issue__id=int(pk), region__id=int(request.user.region_id)).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', region__id=int(request.user.region_id), issue__id=int(pk)).count(),
            }
            
            
            num_of_unresolved = IssueComment.objects.filter(responded_to=False, region__id=int(request.user.region_id), issue__id=int(pk)).count()
            num_of_resolved = IssueComment.objects.filter(responded_to=True, region__id=int(request.user.region_id), issue__id=int(pk)).count()
            latest = IssueComment.objects.filter(region__id=int(request.user.region_id)).order_by('-id')[:5]


        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            issues = Issue.objects.filter(project__id=3)
            num_of_comments = IssueComment.objects.filter(issue__project__id=3).count()
            channel_graph = {
                'Web': IssueComment.objects.filter(input_channel='Web', issue__project__id=3, issue__id=int(pk)).count(),
                'SMS': IssueComment.objects.filter(input_channel='SMS', issue__project__id=3, issue__id=int(pk)).count(),
                'Email': IssueComment.objects.filter(input_channel='Email', issue__project__id=3, issue__id=int(pk)).count(),
                'WhatsApp': IssueComment.objects.filter(input_channel='WhatsApp', issue__project__id=3, issue__id=int(pk)).count(),
                'Facebook': IssueComment.objects.filter(input_channel='Facebook', issue__project__id=3, issue__id=int(pk)).count(),
                'Twitter': IssueComment.objects.filter(input_channel='Twitter', issue__project__id=3, issue__id=int(pk)).count(),
                'Offline': IssueComment.objects.filter(input_channel='Offline', issue__project__id=3, issue__id=int(pk)).count(),
            }
            
            
            num_of_unresolved = IssueComment.objects.filter(responded_to=False, issue__project__id=3, issue__id=int(pk)).count()
            num_of_resolved = IssueComment.objects.filter(responded_to=True, issue__project__id=3, issue__id=int(pk)).count()
            latest = IssueComment.objects.filter(issue__project__id=3).order_by('-id')[:5]


        myissue = Issue.objects.get(id=int(pk))

        return render(request, 'p4h/visual_issues.html', {
                      'latest': latest,
                      'num_of_comments': num_of_comments,
                      'num_of_resolved': num_of_resolved,
                      'num_of_unresolved': num_of_unresolved,
                      'issues': issues,
                      'channel_graph': channel_graph,
                      'myissue': myissue
                      }
                      )


@login_required
def edit_comment_p4h(request, pk):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':
        myinstance = IssueComment.objects.get(id=int(pk))
        if request.method == 'GET':
            form = P4HStaffCommentForm(error_class=DivErrorList,
                                  instance=myinstance,
                                  )

            return render_to_response("p4h/edit_comment.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
            }, context_instance=RequestContext(request))

        if request.method == 'POST':
            form = P4HStaffCommentForm(request.POST or None,
                                    error_class=DivErrorList,
                                    instance=myinstance,
                                    )
            if form.is_valid():
                my_projects = form.save(commit=False)
                my_projects.save()
                return redirect('/backend/user_comments/?edited=True')
            return render_to_response("p4h/edit_comment.html", {
                "form": form,
                'id': pk,
                'edit': 'True',
            }, context_instance=RequestContext(request))
    else:
        raise Http404


@login_required
def user_comments(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':        
        if request.user.user_permission == '3':
            comments = IssueComment.objects.filter(hospital__id=int(request.user.hospital_id)).order_by('-id')
        elif request.user.user_permission == '4':
            comments = IssueComment.objects.filter(district__id=int(request.user.district_id)).order_by('-id')
        elif request.user.user_permission == '5':
            comments = IssueComment.objects.filter(region__id=int(request.user.region_id)).order_by('-id')
        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            comments = IssueComment.objects.filter(issue__project__id=3).order_by('-id')
        return render_to_response('p4h/all_comments.html', {
            'comments': comments,
        }, context_instance=RequestContext(request))

    else:
        raise Http404


@login_required
def filter_unverified_p4h(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':        
        if request.user.user_permission == '3':
            comments = IssueComment.objects.filter(is_verified=False, hospital__id=int(request.user.hospital_id)).order_by('-id')
        elif request.user.user_permission == '4':
            comments = IssueComment.objects.filter(is_verified=False, district__id=int(request.user.district_id)).order_by('-id')
        elif request.user.user_permission == '5':
            comments = IssueComment.objects.filter(is_verified=False, region__id=int(request.user.region_id)).order_by('-id')
        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            comments = IssueComment.objects.filter(is_verified=False, issue__project__id=3).order_by('-id')
        return render_to_response('p4h/all_comments.html', {
            'comments': comments,
        }, context_instance=RequestContext(request))

    else:
        raise Http404


@login_required
def filter_escalated_p4h(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':        
        if request.user.user_permission == '3':
            comments = IssueComment.objects.filter(is_district_escalated=True, hospital__id=int(request.user.hospital_id)).order_by('-id')
        elif request.user.user_permission == '4':
            comments = IssueComment.objects.filter(is_district_escalated=True, district__id=int(request.user.district_id)).order_by('-id')
        elif request.user.user_permission == '5':
            comments = IssueComment.objects.filter(is_district_escalated=True, region__id=int(request.user.region_id)).order_by('-id')
        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            comments = IssueComment.objects.filter(is_district_escalated=True, issue__project__id=3).order_by('-id')
        return render_to_response('p4h/all_comments.html', {
            'comments': comments,
        }, context_instance=RequestContext(request))

    else:
        raise Http404


@login_required
def filter_verified_pending_p4h(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':        
        if request.user.user_permission == '3':
            comments = IssueComment.objects.filter(is_verified=True,  is_resolved=False, hospital__id=int(request.user.hospital_id)).order_by('-id')
        elif request.user.user_permission == '4':
            comments = IssueComment.objects.filter(is_verified=True,  is_resolved=False, district__id=int(request.user.district_id)).order_by('-id')
        elif request.user.user_permission == '5':
            comments = IssueComment.objects.filter(is_verified=True,  is_resolved=False, region__id=int(request.user.region_id)).order_by('-id')
        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            comments = IssueComment.objects.filter(is_verified=True,  is_resolved=False, issue__project__id=3).order_by('-id')
        return render_to_response('p4h/all_comments.html', {
            'comments': comments,
        }, context_instance=RequestContext(request))

    else:
        raise Http404


@login_required
def filter_resolved_p4h(request):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':        
        if request.user.user_permission == '3':
            comments = IssueComment.objects.filter(is_resolved=True, hospital__id=int(request.user.hospital_id)).order_by('-id')
        elif request.user.user_permission == '4':
            comments = IssueComment.objects.filter(is_resolved=True, district__id=int(request.user.district_id)).order_by('-id')
        elif request.user.user_permission == '5':
            comments = IssueComment.objects.filter(is_resolved=True, region__id=int(request.user.region_id)).order_by('-id')
        elif request.user.user_permission == '6' or request.user.user_permission == '7':
            comments = IssueComment.objects.filter(is_resolved=True, issue__project__id=3).order_by('-id')
        return render_to_response('p4h/all_comments.html', {
            'comments': comments,
        }, context_instance=RequestContext(request))

    else:
        raise Http404


@login_required
def view_comment_p4h(request, pk):
    if request.user.user_permission == '3' or request.user.user_permission == '4' or request.user.user_permission == '5' or request.user.user_permission == '6' or request.user.user_permission == '7':
        mycomment = IssueComment.objects.get(id=int(pk))
        if request.method == 'GET':
            return render_to_response('p4h/comment_detail.html', {'mycomment': mycomment,
                                                           }, context_instance=RequestContext(request))
    else:
        raise Http404


def p4h_backend_login(request):
    if request.user.is_authenticated():
        return redirect('/users/p4h_dashboard/')
    next = request.GET.get('next')
    form = LoginForm()
    return render(request, 'p4h/backend_login.html', {'form': form, 'next': next})


def p4h_home(request):
    # send_message_single('0246882768', 'Mr. Okuampah', None, True)
    return render(request, 'p4h/home.html', {
        'regions': Region.objects.all().order_by('name'),
        'policies': Issue.objects.filter(project__id=3).order_by('title'),
        'sliders': Slider.objects.all().order_by('-id'),
        'approaches': Approach.objects.all().order_by('-id'),
        'shortnews': News.objects.all().order_by('-id')[:2],
    })


def frontend_home(request):
    issues = Issue.objects.filter(project__id=1).order_by('-id')[:3]
    excellent = IssueComment.objects.filter(rating__name='Excellent').order_by('-id')[:4]
    excellent_count =  IssueComment.objects.filter(rating__name='Excellent').count()
    good = IssueComment.objects.filter(rating__name='Good').order_by('-id')[:4]
    good_count = IssueComment.objects.filter(rating__name='Good').count()
    neutral = IssueComment.objects.filter(rating__name='Neutral').order_by('-id')[:4]
    neutral_count = IssueComment.objects.filter(rating__name='Neutral').count()
    bad = IssueComment.objects.filter(rating__name='Bad').order_by('-id')[:4]
    bad_count = IssueComment.objects.filter(rating__name='Bad').count()
    document = KnowledgeDocument.objects.filter(project__id=1, show_on_homepage=True).order_by('-id')[:4]
    return render(request,
                  'frontend_index.html',
                  {'issues': issues,
                   'document': document,
                   'excellent': excellent,
                   'excellent_count': excellent_count,
                   'good': good,
                   'good_count': good_count,
                   'neutral': neutral,
                   'neutral_count': neutral_count,
                   'bad': bad,
                   'bad_count': bad_count,
                   'total_count': bad_count + good_count + neutral_count + excellent_count,
                   'relevant': RelevantLink.objects.all()
    })


def my_issue_filter(request):
    selected_id = int(request.POST['issue_id'])
    if selected_id == 0:
        excellent = IssueComment.objects.filter(rating__name='Excellent').order_by('-id')[:4]
        excellent_count =  IssueComment.objects.filter(rating__name='Excellent').count()
        good = IssueComment.objects.filter(rating__name='Good').order_by('-id')[:4]
        good_count = IssueComment.objects.filter(rating__name='Good').count()
        neutral = IssueComment.objects.filter(rating__name='Neutral').order_by('-id')[:4]
        neutral_count = IssueComment.objects.filter(rating__name='Neutral').count()
        bad = IssueComment.objects.filter(rating__name='Bad').order_by('-id')[:4]
        bad_count = IssueComment.objects.filter(rating__name='Bad').count()
    else:
        excellent = IssueComment.objects.filter(issue__id=selected_id, rating__name='Excellent').order_by('-id')[:4]
        excellent_count =  IssueComment.objects.filter(issue__id=selected_id, rating__name='Excellent').count()
        good = IssueComment.objects.filter(issue__id=selected_id, rating__name='Good').order_by('-id')[:4]
        good_count = IssueComment.objects.filter(issue__id=selected_id, rating__name='Good').count()
        neutral = IssueComment.objects.filter(issue__id=selected_id, rating__name='Neutral').order_by('-id')[:4]
        neutral_count = IssueComment.objects.filter(issue__id=selected_id, rating__name='Neutral').count()
        bad = IssueComment.objects.filter(issue__id=selected_id, rating__name='Bad').order_by('-id')[:4]
        bad_count = IssueComment.objects.filter(issue__id=selected_id, rating__name='Bad').count()        
    return render(request, 'issue_ajax.html',
                  {'excellent': excellent,
                   'excellent_count': excellent_count,
                   'good': good,
                   'good_count': good_count,
                   'neutral': neutral,
                   'neutral_count': neutral_count,
                   'selected_id': selected_id,
                   'bad': bad,
                   'bad_count': bad_count,
                   'total_count': bad_count + good_count + neutral_count + excellent_count,
                   'issues': Issue.objects.filter(project__id=1).order_by('-id')[:3]
    })

def about(request):
    return render(request, 'about.html')


def p4h_about(request):
    return render(request, 'p4h/about.html', {'shortnews': News.objects.all().order_by('-id')[:2], 'about': About.objects.all()})


def p4h_contact(request):
    return render(request, 'p4h/contact.html', {'shortnews': News.objects.all().order_by('-id')[:2], 'contact': Contact.objects.all()})


def p4h_news(request):
    mynews = News.objects.all().order_by('-id')
    return render(request, 'p4h/news.html', {'mynews':mynews, 'shortnews': mynews[:2], 'othernews': mynews[:3]})


def knowledgehub_articles(request):
    articles = KnowledgeArticle.objects.filter(project__id=3).order_by('-id')
    return render(request, 'p4h/knowledgehub_articles.html', {'articles':articles, 'shortnews': News.objects.all().order_by('-id')[:2], 'othernews': News.objects.all().order_by('-id')[:3]})


def knowledgehub_documents(request):
    documents = KnowledgeDocument.objects.filter(project__id=3).order_by('-id')
    return render(request, 'p4h/knowledgehub_documents.html', {'documents':documents, 'shortnews': News.objects.all().order_by('-id')[:2], 'othernews': News.objects.all().order_by('-id')[:3]})


def p4h_comments(request):
    comments = IssueComment.objects.filter(issue__project__id=3, is_verified=True, verification_truth=True)
    return render(request, 'p4h/comments.html', {'comments': comments,
        'regions': Region.objects.all().order_by('name'),
        'policies': Issue.objects.filter(project__id=3).order_by('title'),
        'shortnews': News.objects.all().order_by('-id')[:2],
    })


def p4h_regions(request):
    return render(request, 'p4h/regions.html', {'shortnews': News.objects.all().order_by('-id')[:2],})


def p4h_policy(request):
    return render(request, 'p4h/policy.html', {'policies': Issue.objects.filter(project__id=3), 'shortnews': News.objects.all().order_by('-id')[:2],})


def frontend_gallery_videos(request):
    videos = GalleryVideo.objects.all()
    return render(request, 'videos.html', {'videos': videos, 'relevant': RelevantLink.objects.all()})


def frontend_gallery_pictures(request):
    albums = GalleryAlbum.objects.all()
    return render(request, 'pictures.html', {'albums': albums, 'relevant': RelevantLink.objects.all()})


def p4h_news_detail(request, pk):
    myobject = News.objects.get(id=int(pk))
    return render(request, 'p4h/news_detail.html', {'myobject': myobject, 'shortnews': News.objects.all().order_by('-id')[:2],})


def knowledgehub_articles_detail(request, pk):
    myobject = KnowledgeArticle.objects.get(project__id=3, id=int(pk))
    return render(request, 'p4h/knowledgehub_articles_detail.html', {'myobject': myobject, 'shortnews': News.objects.all().order_by('-id')[:2],})


def frontend_documents(request):
    documents = KnowledgeDocument.objects.filter(project__id=1)
    return render(request, 'documents.html', {'documents': documents, 'relevant': RelevantLink.objects.all()})


def frontend_articles(request):
    articles = KnowledgeArticle.objects.filter(project__id=1).order_by('-date_published')
    return render(request, 'articles.html', {'articles': articles, 'relevant': RelevantLink.objects.all()})


def post_comment(request):
    if request.method == "POST":
        issue_id = request.POST['issue_id']
        comment = request.POST['comment']
        image = request.POST['image']
        video = request.POST['video']
        rating_id = request.POST['rating']
        location = request.POST['location']
        media_url = request.POST.get('media_url', None)

        issue_resolved = True
        if CommentCategory.objects.get(id=int(rating_id)).value < 3:
            issue_resolved = False

        IssueComment.objects.create(issue=Issue.objects.get(id=int(issue_id)),
                                    image=image,
                                    video=video,
                                    comment=comment,
                                    input_channel='Web',
                                    author=request.user,
                                    media_url=media_url,
                                    location=location,
                                    issue_resolved=issue_resolved,
                                    rating=CommentCategory.objects.get(id=int(rating_id)))
        try:
            if CommentCategory.objects.get(id=int(rating_id)).value < 3:
                send_mail(
                    'New Issue Comment that needs attention',
                    comment,
                    'Social Accountability <gidboa@gmail.com>',
                    ['info@penplusbytes.org'],
                    fail_silently=False,
                )
        except:
            pass
        return redirect('/issues/%s' % issue_id)


def p4h_post_comment(request):
    if request.method == "POST":
        issue_id = request.POST['issue_id']
        comment = request.POST['comment']
        image = request.POST['image']
        video = request.POST['video']
        rating_id = request.POST['rating']
        media_url = request.POST.get('media_url', None)

        issue_resolved = True
        if CommentCategory.objects.get(id=int(rating_id)).value < 3:
            issue_resolved = False

        IssueComment.objects.create(issue=Issue.objects.get(id=int(issue_id)),
                                    image=image,
                                    video=video,
                                    comment=comment,
                                    input_channel='Web',
                                    author=request.user,
                                    media_url=media_url,
                                    issue_resolved=issue_resolved,
                                    rating=CommentCategory.objects.get(id=int(rating_id)))
        try:
            if CommentCategory.objects.get(id=int(rating_id)).value < 3:
                send_mail(
                    'New Social Accountability Comment needs attention',
                    comment,
                    'Kakum <gidboa@gmail.com>',
                    ['info@penplusbytes.org'],
                    fail_silently=False,
                )
        except:
            pass
        return redirect('/comments/')


def issues(request):
    issues = Issue.objects.filter(project__id=1)
    return render(request, 'issues.html', {'issues': issues, 'relevant': RelevantLink.objects.all()})


def issue_detail(request, pk):
    myissue = Issue.objects.get(id=int(pk))
    rating = CommentCategory.objects.all()
    videos = myissue.get_videos()
    images = myissue.get_images()
    documents = myissue.get_documents()
    return render(request, 'issue_detail_frontend.html', {'myissue': myissue,
                                                          'images': images,
                                                          'ratings': rating,
                                                          'documents': documents,
                                                          'relevant': RelevantLink.objects.all(),
                                                          'videos': videos})


def article_detail(request, pk):
    myarticle = KnowledgeArticle.objects.get(id=int(pk))
    myarticles = KnowledgeArticle.objects.filter(project__id=1).order_by('-date_published').exclude(id=myarticle.id)[:4]
    return render(request, 'frontend_article_detail.html', {'myarticle': myarticle, 'relevant': RelevantLink.objects.all(), 'myarticles': myarticles})


def view_status_p4h(request, pk):
    return render(request, 'p4h/view_status.html', {
        'mycomment': IssueComment.objects.get(id=int(pk)),
        'verify_form': P4HCommentVerifyForm(),
        'resolve_form': P4HResolveForm(),
        'district_escalate_form': P4HDistrictEscalateForm(),
        'regional_escalate_form': P4HRegionalEscalateForm(),
        'national_escalate_form': P4HNationalEscalateForm(),
    })


def submitted_comments(request):
    return render(request, 'p4h/submitted_comments.html', {
        'comments': IssueComment.objects.filter(author=request.user),
    })


def login_view(request):
    next = request.GET.get('next')
    if request.user.is_authenticated():
        redirect('/issues/')

    if request.method == "POST":
        form = LoginForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            userobject = authenticate(email=email, password=password)
            if userobject is not None and userobject.is_active:
                login(request, userobject)
                if request.POST.get('next'):
                    return redirect(request.POST.get('next'))
                return redirect('/issues/')
            else:
                return render(request, 'login.html', {'form': form})
        else:
            return render(request, 'login.html', {'form': form})

    ''' user is not submitting any form, show the login form '''
    form = LoginForm()
    return render(request, 'login.html', {'form': form, 'next': next})


def p4h_login_view(request):
    next = request.GET.get('next')
    if request.user.is_authenticated():
        redirect('/submitted_comments/')

    if request.method == "POST":
        form = LoginForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            userobject = authenticate(email=email, password=password)
            if userobject is not None and userobject.is_active:
                login(request, userobject)
                return redirect('/submitted_comments/')
            else:
                return render(request, 'p4h/login.html', {'form': form})
        else:
            return render(request, 'p4h/login.html', {'form': form})

    ''' user is not submitting any form, show the login form '''
    form = LoginForm()
    return render(request, 'p4h/login.html', {
        'form': form,
        'next': next,
        'shortnews': News.objects.all().order_by('-id')[:2]
    })


def register_view(request):
    next_param = request.GET.get('next')

    if request.user.is_authenticated():
        redirect('/')

    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        next_param = request.POST.get('next')

        try:
            KAKUser.objects.get(email=email)
            return render(request, 'register.html', {'next': next_param,
                                                     'error': 'Email Already Exist'
                                                     })
        except:
            user = KAKUser(
                email=email,
                username='username',
                first_name=first_name,
                last_name=last_name,
                signup_method='Email'
            )
            user.set_password(password)
            user.save()
            user = authenticate(email=user.email, password=password)
            login(request, user)
            if request.POST.get('next'):
                return redirect(request.POST.get('next'))
            return redirect('/')

    return render(request, 'register.html', {'next': next_param})


def p4h_register_view(request):
    next_param = request.GET.get('next')

    if request.user.is_authenticated():
        redirect('/submitted_comments/')

    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        gender = request.POST.get('gender', None)
        age = request.POST.get('age', None)
        next_param = request.POST.get('next')

        try:
            KAKUser.objects.get(email=email)
            return render(request, 'p4h/register.html', {'next': next_param,
                                                     'error': 'Email Already Exist'
                                                     })
        except:
            user = KAKUser(
                email=email,
                username='username',
                first_name=first_name,
                last_name=last_name,
                is_p4h_user=True,
                gender=gender,
                age=age,
                signup_method='Email'
            )
            user.set_password(password)
            user.save()
            user = authenticate(email=user.email, password=password)
            login(request, user)
            if request.POST.get('next'):
                return redirect(request.POST.get('next'))
            return redirect('/submitted_comments/')

    return render(request, 'p4h/register.html', {'next': next_param, 'shortnews': News.objects.all().order_by('-id')[:2]})


def logout_view(request):
    logout(request)
    return redirect('/')


def p4h_logout_view(request):
    logout(request)
    return redirect('/')


def p4h_facebook_register(request):

    if request.method == "POST":
        email = request.POST['email']
        fullname = request.POST['fullname']
        first_name = fullname.split(' ')[0]
        last_name = fullname.split(' ')[1]
        image_url = request.POST.get('image_url')
        password = request.POST.get('password')

        try:
            KAKUser.objects.get(email=email)
            user = authenticate(email=email, password=password)
            login(request, user)
            return redirect('/submitted_comments/')
        except:
            try:
                user = KAKUser(
                    email=email,
                    username='username',
                    first_name=first_name,
                    last_name=last_name,
                    sm_avatar=image_url,
                    is_p4h_user=True,
                    signup_method='Facebook'
                )
                user.set_password(password)
                user.save()
                user = authenticate(email=user.email, password=password)
                login(request, user)
                return redirect('/comments/')
            except:
                myuser = KAKUser.objects.get(email=email)
                myuser.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, myuser)
                return redirect('/submitted_comments/')

    return render(request, 'p4h/register.html')


def facebook_register(request):

    if request.method == "POST":
        email = request.POST['email']
        fullname = request.POST['fullname']
        first_name = fullname.split(' ')[0]
        last_name = fullname.split(' ')[1]
        image_url = request.POST.get('image_url')
        password = request.POST.get('password')

        try:
            KAKUser.objects.get(email=email)
            user = authenticate(email=email, password=password)
            login(request, user)
            return redirect('/')
        except:
            try:
                user = KAKUser(
                    email=email,
                    username='username',
                    first_name=first_name,
                    last_name=last_name,
                    sm_avatar=image_url,
                    signup_method='Facebook'
                )
                user.set_password(password)
                user.save()
                user = authenticate(email=user.email, password=password)
                login(request, user)
                return redirect('/')
            except:
                myuser = KAKUser.objects.get(email=email)
                myuser.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, myuser)
                return redirect('/')

    return render(request, 'register.html')


def google_register(request):

    if request.method == "POST":
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        image_url = request.POST.get('image_url')
        password = request.POST.get('password')

        try:
            KAKUser.objects.get(email=email)
            user = authenticate(email=email, password=password)
            login(request, user)
            return redirect('/issues/')
        except:
            try:
                user = KAKUser(
                    email=email,
                    username='username',
                    first_name=first_name,
                    last_name=last_name,
                    sm_avatar=image_url,
                    signup_method='Google'
                )
                user.set_password(password)
                user.save()
                user = authenticate(email=user.email, password=password)
                login(request, user)
                return redirect('/')
            except:
                myuser = KAKUser.objects.get(email=email)
                myuser.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, myuser)
                return redirect('/')


    return render(request, 'register.html')


def p4h_google_register(request):

    if request.method == "POST":
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        image_url = request.POST.get('image_url')
        password = request.POST.get('password')

        try:
            KAKUser.objects.get(email=email)
            user = authenticate(email=email, password=password)
            login(request, user)
            return redirect('/submitted_comments/')
        except:
            try:
                user = KAKUser(
                    email=email,
                    username='username',
                    first_name=first_name,
                    last_name=last_name,
                    sm_avatar=image_url,
                    is_p4h_user=True,
                    signup_method='Google'
                )
                user.set_password(password)
                user.save()
                user = authenticate(email=user.email, password=password)
                login(request, user)
                return redirect('/submitted_comments/')
            except:
                myuser = KAKUser.objects.get(email=email)
                myuser.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, myuser)
                return redirect('/comments/')

    return render(request, 'p4h/register.html')


def submit_p4h_comment(request):
    if request.method == "POST":
        issue_id = request.POST['issue_id']
        region_id = request.POST['region_id']
        district_id = request.POST['district_id']
        hospital_id = request.POST['hospital_id']
        comment = request.POST['comment']
        phone_number = request.POST['phone_number']
        image = request.POST['image']
        audio = request.POST['audio']
        video = request.POST['video']

        if request.user.is_authenticated():
            author = request.user
        else:
            author = None

        if phone_number:
            try:
                send_message_single(phone_number, 'Your feedback has been received. You will be updated on the status of this feedback. Thank you', None, True)
            except:
                pass

        duty_bearer = KAKUser.objects.filter(hospital_id=str(hospital_id))
        if duty_bearer:
            duty_bearer = duty_bearer[0]
            try:
                send_mail(
                    'New Comment from P4H',
                    comment,
                    'People 4 Health <gidboa@gmail.com>',
                    [duty_bearer.email, ],
                    fail_silently=False,
                )
            except:
                pass
        else:
            duty_bearer = None

        IssueComment.objects.create(issue=Issue.objects.get(id=int(issue_id)),
                                    image=image,
                                    video=video,
                                    audio=audio,
                                    region=Region.objects.get(id=int(region_id)),
                                    district=District.objects.get(id=int(district_id)),
                                    hospital=Hospital.objects.get(id=int(hospital_id)),
                                    phone_number=phone_number,
                                    comment=comment,
                                    input_channel='Web',
                                    author=author,
                                    duty_bearer=duty_bearer
                                    )
        return redirect('/comments/?added=True')


def region_filter(request):
    if request.method == "POST":
        region_id = request.POST['region_id']
        districts = District.objects.filter(region__id=int(region_id)).order_by('name')
        return render(request, 'p4h/district_ajax.html', {'districts': districts})


def submit_newsletter(request):
    if request.method == "POST":
        email = request.POST['email']
        Newsletter.objects.create(email=email)
        return render(request, 'p4h/thank.html')


def district_filter(request):
    if request.method == "POST":
        district_id = request.POST['district_id']
        hospitals = Hospital.objects.filter(district__id=int(district_id)).order_by('name')
        return render(request, 'p4h/hospital_ajax.html', {'hospitals': hospitals})


def verify_comment(request, pk):
    if request.method == "POST":
        myinstance = IssueComment.objects.get(id=int(pk))
        form = P4HCommentVerifyForm(request.POST or None,
                                    error_class=DivErrorList,
                                    instance=myinstance,
                                    )
        if form.is_valid():
            my_comment = form.save(commit=False)
            my_comment.is_verified = True
            my_comment.date_verified = datetime.now()
            my_comment.save()
            return redirect('/backend/view_status_p4h/%s/' % pk)

        return render_to_response("p4h/view_status.html", {
                "verify_form": form,
                "mycomment": IssueComment.objects.get(id=int(pk))
            }, context_instance=RequestContext(request))


def resolve_comment(request, pk):
    if request.method == "POST":
        myinstance = IssueComment.objects.get(id=int(pk))
        form = P4HResolveForm(request.POST or None,
                                    error_class=DivErrorList,
                                    instance=myinstance,
                                    )
        if form.is_valid():
            my_comment = form.save(commit=False)
            my_comment.is_resolved = True
            my_comment.date_resolved = datetime.now()
            my_comment.save()
            if myinstance.phone_number:
                try:
                    send_message_single(myinstance.phone_number, 'Dear User, Your feedback you submitted on %s has been resolved' % myinstance.timestamp, None, True)
                except:
                    pass
            return redirect('/backend/view_status_p4h/%s/' % pk)

        return render_to_response("p4h/view_status.html", {
                "resolve_form": form,
                "mycomment": IssueComment.objects.get(id=int(pk))
            }, context_instance=RequestContext(request))


def escalate_to_district(request, pk):
    if request.method == "POST":
        myinstance = IssueComment.objects.get(id=int(pk))
        form = P4HDistrictEscalateForm(request.POST or None,
                                    error_class=DivErrorList,
                                    instance=myinstance,
                                    )
        if form.is_valid():
            my_comment = form.save(commit=False)
            my_comment.is_district_escalated = True
            my_comment.hospital_escalation_date = datetime.now()
            district_escalant = KAKUser.objects.filter(user_permission='4', district_id=str(my_comment.district.id))
            if district_escalant:
                district_escalant = district_escalant[0]
                my_comment.district_escalant = district_escalant
                try:
                    send_mail(
                        'New Comment from P4H',
                        my_comment.hospital_escalation_comment,
                        'People 4 Health <gidboa@gmail.com>',
                        [district_escalant.email, ],
                        fail_silently=False,
                    )
                except:
                    pass
                my_comment.save()
                return redirect('/backend/view_status_p4h/%s/' % pk)
            else:
                return render_to_response("p4h/view_status.html", {
                    "no_district_monitor": True,
                    "mycomment": IssueComment.objects.get(id=int(pk))
                }, context_instance=RequestContext(request))

        return render_to_response("p4h/view_status.html", {
                "district_escalate_form": form,
                'verify_form': P4HCommentVerifyForm(),
                'resolve_form': P4HResolveForm(),
                'regional_escalate_form': P4HRegionalEscalateForm(),
                'national_escalate_form': P4HNationalEscalateForm(),
                "mycomment": IssueComment.objects.get(id=int(pk))
            }, context_instance=RequestContext(request))


def escalate_to_regional(request, pk):
    if request.method == "POST":
        myinstance = IssueComment.objects.get(id=int(pk))
        form = P4HRegionalEscalateForm(request.POST or None,
                                    error_class=DivErrorList,
                                    instance=myinstance,
                                    )
        if form.is_valid():
            my_comment = form.save(commit=False)
            my_comment.is_regional_escalated = True
            my_comment.district_escalation_date = datetime.now()
            regional_escalant = KAKUser.objects.filter(user_permission='5', region_id=str(my_comment.region.id))
            if regional_escalant:
                regional_escalant = regional_escalant[0]
                my_comment.regional_escalant = regional_escalant
                try:
                    send_mail(
                        'New Comment from P4H',
                        mycomment.district_escalation_comment,
                        'People 4 Health <gidboa@gmail.com>',
                        [regional_escalant.email, ],
                        fail_silently=False,
                    )
                except:
                    pass
                my_comment.save()
                return redirect('/backend/view_status_p4h/%s/' % pk)
            else:
                return render_to_response("p4h/view_status.html", {
                    "no_regional_monitor": True,
                    "mycomment": IssueComment.objects.get(id=int(pk))
                }, context_instance=RequestContext(request))

        return render_to_response("p4h/view_status.html", {
                "regional_escalate_form": form,
                'verify_form': P4HCommentVerifyForm(),
                'resolve_form': P4HResolveForm(),
                'district_escalate_form': P4HDistrictEscalateForm(),
                'national_escalate_form': P4HNationalEscalateForm(),
                "mycomment": IssueComment.objects.get(id=int(pk))
            }, context_instance=RequestContext(request))


def escalate_to_national(request, pk):
    if request.method == "POST":
        myinstance = IssueComment.objects.get(id=int(pk))
        form = P4HNationalEscalateForm(request.POST or None,
                                    error_class=DivErrorList,
                                    instance=myinstance,
                                    )
        if form.is_valid():
            my_comment = form.save(commit=False)
            my_comment.is_national_escalated = True
            my_comment.regional_escalation_date = datetime.now()
            national_escalant = KAKUser.objects.filter(user_permission='6')
            if national_escalant:
                national_escalant = national_escalant[0]
                my_comment.national_escalant = national_escalant
                try:
                    send_mail(
                        'New Comment from P4H',
                        mycomment.district_escalation_comment,
                        'People 4 Health <gidboa@gmail.com>',
                        [national_escalant.email, ],
                        fail_silently=False,
                    )
                except:
                    pass
                my_comment.save()
                return redirect('/backend/view_status_p4h/%s/' % pk)
            else:
                return render_to_response("p4h/view_status.html", {
                    "no_national_monitor": True,
                    "mycomment": IssueComment.objects.get(id=int(pk))
                }, context_instance=RequestContext(request))

        return render_to_response("p4h/view_status.html", {
                "regional_escalate_form": P4HRegionalEscalateForm(),
                'verify_form': P4HCommentVerifyForm(),
                'resolve_form': P4HResolveForm(),
                'district_escalate_form': P4HDistrictEscalateForm(),
                'national_escalate_form': form,
                "mycomment": IssueComment.objects.get(id=int(pk))
            }, context_instance=RequestContext(request))


def project_areas(request):
    return render(request, 'project_areas.html', {'relevant': RelevantLink.objects.all()})


def project_areas_ashaiman(request):
    return render(request, 'ashaiman.html', {'relevant': RelevantLink.objects.all()})


def project_areas_ellembelle(request):
    return render(request, 'ellembelle.html', {'relevant': RelevantLink.objects.all()})
