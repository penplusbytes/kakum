from django.conf.urls import patterns, include, url, handler404, handler500
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView
import os
admin.autodiscover()


if 'P4H' in os.environ:
    urlpatterns = patterns('',
        url(r'^$', 'kakum_app.views.p4h_home', name='p4h_home'),
        url(r'^policy_areas/$', 'kakum_app.views.p4h_policy', name='p4h_policy'),
        url(r'^submitted_comments/$', 'kakum_app.views.submitted_comments', name='submitted_comments'),
        url(r'^2fa_confirm/$', 'apps.users.views.twofa_confirm', name='2fa_confirm'),
        url(r'^submit_newsletter/$', 'kakum_app.views.submit_newsletter', name='submit_newsletter'),
        url(r'^regions/$', 'kakum_app.views.p4h_regions', name='regions'),
        url(r'^backend/$', 'kakum_app.views.p4h_backend_login', name='p4h_backend_login'),
        url(r'^backend/user_comments/$', 'kakum_app.views.user_comments', name='user_comments'),
        url(r'^backend/filter_unverified_p4h/$', 'kakum_app.views.filter_unverified_p4h', name='filter_unverified_p4h'),
        url(r'^backend/filter_escalated_p4h/$', 'kakum_app.views.filter_escalated_p4h', name='filter_escalated_p4h'),
        url(r'^backend/filter_verified_pending_p4h/$', 'kakum_app.views.filter_verified_pending_p4h', name='filter_verified_pending_p4h'),
        url(r'^backend/filter_resolved_p4h/$', 'kakum_app.views.filter_resolved_p4h', name='filter_resolved_p4h'),
        
        url(r'^backend/p4h_visualisations/$', 'kakum_app.views.issues_visualisations', name='p4h_visualisations'),
        # url(r'^p4h_gallery/videos/$', 'kakum_app.views.p4h_gallery_videos', name='p4h_videos'),
        # url(r'^gallery/pictures/$', 'kakum_app.views.frontend_gallery_pictures', name='frontend_pictures'),
        url(r'^user_login/$', 'kakum_app.views.p4h_login_view', name='login_view'),
        url(r'^user_logout/$', 'kakum_app.views.p4h_logout_view', name='logout_view'),
        url(r'^google_register/$', 'kakum_app.views.p4h_google_register', name='google_register'),
        url(r'^facebook_register/$', 'kakum_app.views.p4h_facebook_register', name='facebook_register'),
        url(r'^submit_p4h_comment/$', 'kakum_app.views.submit_p4h_comment', name='submit_p4h_comment'),
        url(r'^user_register/$', 'kakum_app.views.p4h_register_view', name='register_view'),
        # url(r'^knowledge_center/audios/$', 'kakum_app.views.frontend_audios', name='frontend_audios'),
        # url(r'^knowledge_center/articles/$', 'kakum_app.views.frontend_articles', name='frontend_articles'),
        # url(r'^knowledge_center/documents/$', 'kakum_app.views.frontend_documents', name='frontend_documents'),
        # url(r'^issues/(?P<pk>\d+)/$', 'kakum_app.views.issue_detail', name='issue_detail'),
        # url(r'^articles/(?P<pk>\d+)/$', 'kakum_app.views.article_detail', name='article_detail'),
        url(r'^comments/$', 'kakum_app.views.p4h_comments', name='comments'),
        url(r'^backend/view_comment_p4h/(?P<pk>\d+)/$', 'kakum_app.views.view_comment_p4h', name='view_comment_p4h'),
        url(r'^backend/view_status_p4h/(?P<pk>\d+)/$', 'kakum_app.views.view_status_p4h', name='view_status_p4h'),
        url(r'^backend/verify_comment/(?P<pk>\d+)/$', 'kakum_app.views.verify_comment', name='verify_comment'),
        url(r'^backend/resolve_comment/(?P<pk>\d+)/$', 'kakum_app.views.resolve_comment', name='resolve_comment'),
        url(r'^backend/escalate_to_district/(?P<pk>\d+)/$', 'kakum_app.views.escalate_to_district', name='escalate_to_district'),
        url(r'^backend/escalate_to_regional/(?P<pk>\d+)/$', 'kakum_app.views.escalate_to_regional', name='escalate_to_regional'),
        url(r'^backend/escalate_to_national/(?P<pk>\d+)/$', 'kakum_app.views.escalate_to_national', name='escalate_to_national'),
        url(r'^district_filter/$', 'kakum_app.views.district_filter', name='district_filter'),
        url(r'^region_filter/$', 'kakum_app.views.region_filter', name='region_filter'),
        url(r'^backend/issues_visualisations/(?P<pk>\d+)/$', 'kakum_app.views.issues_visualisations_details', name="p4h_issues_visualisations_details"),
        url(r'^contact/$', 'kakum_app.views.p4h_contact', name='contact'),
        url(r'^message/$', 'apps.smsapp.views.receive_message', name='receive_message'),
        url(r'^news/$', 'kakum_app.views.p4h_news', name='news'),
        url(r'^news/(?P<pk>\d+)/$', 'kakum_app.views.p4h_news_detail', name='news_detailed'),
        url(r'^knowledgehub_articles/(?P<pk>\d+)/$', 'kakum_app.views.knowledgehub_articles_detail', name='knowledgehub_articles_detailed'),
        url(r'^knowledgehub_articles/$', 'kakum_app.views.knowledgehub_articles', name='knowledgehub_articles'),
        url(r'^knowledgehub_documents/$', 'kakum_app.views.knowledgehub_documents', name='knowledgehub_documents'),
        url(r'^articles/(?P<pk>\d+)/$', 'kakum_app.views.knowledgehub_articles_detail', name='knowledgehub_articles_detail'),        
        url(r'^about/$', 'kakum_app.views.p4h_about', name='about'),
        url(r'^users/', include('apps.users.urls')),
        url(r'^login/$', 'kakum_app.views.p4h_login_view', name="login"),
        url(r'^s3direct/', include('s3direct.urls')),
        # url(r'^api/v1.0/', include('apps.rest_api.urls', namespace='api')),
        url(r'^backend_admin/', include(admin.site.urls)),
        # url(r'^docs/', include('rest_framework_swagger.urls')),
        url(r'^tinymce/', include('tinymce.urls')),

        #python social auth things
        url('', include('social.apps.django_app.urls', namespace='social')),
        url('', include('django.contrib.auth.urls', namespace='auth')),
        url(r'^projects/', include('apps.projects.urls')),
        #change password
        url(r'^password_change/$', 'apps.users.views.password_change', name='password_change'),
        url(r'^password_change_done/$', 'apps.users.views.password_change_done', name='password_change_done'),

        #password change
        url(r'^resetpassword/passwordsent/$','django.contrib.auth.views.password_reset_done', name='password_reset_done'),
        url(r'^resetpassword/$','django.contrib.auth.views.password_reset', name='password_reset'),
        url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$','django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
        url(r'^reset/done/$','django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),
        url(r'^direct/$', TemplateView.as_view, {'template': 'direct.html','extra_context':{'showDirect':True}}),
    )

else:
    urlpatterns = patterns('',
        url(r'^$', 'kakum_app.views.frontend_home', name='frontend_home'),
        url(r'^backend/$', 'kakum_app.views.backend_home', name='backend_home'),
        url(r'^gallery/videos/$', 'kakum_app.views.frontend_gallery_videos', name='frontend_videos'),
        url(r'^gallery/pictures/$', 'kakum_app.views.frontend_gallery_pictures', name='frontend_pictures'),
        url(r'^user_login/$', 'kakum_app.views.login_view', name='login_view'),
        url(r'^user_logout/$', 'kakum_app.views.logout_view', name='logout_view'),
        url(r'^google_register/$', 'kakum_app.views.google_register', name='google_register'),
        url(r'^facebook_register/$', 'kakum_app.views.facebook_register', name='facebook_register'),
        url(r'^post_comment/$', 'kakum_app.views.post_comment', name='post_comment'),
        url(r'^user_register/$', 'kakum_app.views.register_view', name='register_view'),
        # url(r'^knowledge_center/audios/$', 'kakum_app.views.frontend_audios', name='frontend_audios'),
        url(r'^knowledge_center/articles/$', 'kakum_app.views.frontend_articles', name='frontend_articles'),
        url(r'^knowledge_center/documents/$', 'kakum_app.views.frontend_documents', name='frontend_documents'),
        url(r'^issues/(?P<pk>\d+)/$', 'kakum_app.views.issue_detail', name='issue_detail'),
        url(r'^articles/(?P<pk>\d+)/$', 'kakum_app.views.article_detail', name='article_detail'),
        url(r'^issues/$', 'kakum_app.views.issues', name='issues'),
        url(r'^about/$', 'kakum_app.views.about', name='about'),
        url(r'^users/', include('apps.users.urls')),

        url(r'^district_filter/$', 'kakum_app.views.district_filter', name='district_filter'),
        url(r'^my_issue_filter/$', 'kakum_app.views.my_issue_filter', name='my_issue_filter'),
        url(r'^region_filter/$', 'kakum_app.views.region_filter', name='region_filter'),
        url(r'^project_areas/$', 'kakum_app.views.project_areas', name='project_areas'),
        url(r'^project_areas/ashaiman/$', 'kakum_app.views.project_areas_ashaiman', name='project_areas_ashaiman'),
        url(r'^project_areas/ellembelle/$', 'kakum_app.views.project_areas_ellembelle', name='project_areas_ellembelle'),
        url(r'^projects/', include('apps.projects.urls')),
        url(r'^login/$', 'apps.users.views.login_view', name="login"),
        url(r'^s3direct/', include('s3direct.urls')),
        url(r'^api/v1.0/', include('apps.rest_api.urls', namespace='api')),
        url(r'^backend_admin/', include(admin.site.urls)),
        url(r'^docs/', include('rest_framework_swagger.urls')),
        url(r'^tinymce/', include('tinymce.urls')),

        #python social auth things
        url('', include('social.apps.django_app.urls', namespace='social')),
        url('', include('django.contrib.auth.urls', namespace='auth')),

        #change password
        url(r'^password_change/$', 'apps.users.views.password_change', name='password_change'),
        url(r'^password_change_done/$', 'apps.users.views.password_change_done', name='password_change_done'),

        #password change
        url(r'^resetpassword/passwordsent/$','django.contrib.auth.views.password_reset_done', name='password_reset_done'),
        url(r'^resetpassword/$','django.contrib.auth.views.password_reset', name='password_reset'),
        url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$','django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
        url(r'^reset/done/$','django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),
        url(r'^direct/$', TemplateView.as_view, {'template': 'direct.html','extra_context':{'showDirect':True}}),
    )


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
        )

handler404 = 'apps.users.views.handler404'

handler500 = 'apps.users.views.handler500'
